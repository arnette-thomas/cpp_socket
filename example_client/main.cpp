#include <iostream>
#include <string>
#include <chrono>
using namespace std::literals::chrono_literals;
#include <thread>
#include <cpp_socket/cpp_socket.hpp>

/**
* Client example
*/
int main(int argc, char* argv[])
{
	// Create Socket
	sock::Socket<sock::Protocol::TCP, sock::AddressType::IPV4> client;

	// Connect to server
	if (!client.Connect("127.0.0.1", 8888))
	{
		std::cerr << "ERROR while connecting to server : " << client.GetLastError() << std::endl;
		return EXIT_FAILURE;
	}

	// Sending "Hello from Client" to server
	// We wait a little before to test non-blocking server
	std::this_thread::sleep_for(3s);
	std::string toSend("Hello from Client");
	auto sentBytes = client.Send(toSend.c_str(), toSend.size());
	if (!sentBytes.has_value())
	{
		std::cerr << "ERROR while sending data to server : " << client.GetLastError() << std::endl;
		return EXIT_FAILURE;
	}
	std::cout << "Sent " << *sentBytes << " bytes to server" << std::endl;


	// Receiving message from server
	char received[1024];
	auto receivedBytes = client.Receive(received, 1024);
	if (!receivedBytes.has_value())
	{
		std::cerr << "ERROR while receiving data from server : " << client.GetLastError() << std::endl;
		return EXIT_FAILURE;
	}
	std::cout << "Received " << *receivedBytes << " bytes from server : " << std::string(received, *receivedBytes) << std::endl;

	std::cin.ignore();

	return EXIT_SUCCESS;
}