#pragma once

#include <optional>
#include <string>
#include <memory>
#include <vector>

// Windows definitions
#ifdef _WIN32

	#define WIN32_LEAN_AND_MEAN
	#define NOMINMAX

	#include <Windows.h>
	#include <WinSock2.h>
	#include <WS2tcpip.h>

	#pragma comment (lib, "Ws2_32.lib")

	#define close(s) closesocket(s)
	#define SHUT_RDWR SD_BOTH
	#define errno() WSAGetLastError()

// UNIX defintions
#else

	#define SOCKET int
	#define INVALID_SOCKET -1

	#include <sys/socket.h>
	#include <netinet/in.h>
	#include<netdb.h>
	#include<arpa/inet.h>

#endif

namespace sock
{
#ifdef _WIN32
	struct WinInitializer
	{
		WinInitializer()
		{
			WSADATA data;
			int err = WSAStartup(MAKEWORD(2, 2), &data);
			if (err)
			{
				
			}
		}

		~WinInitializer()
		{
			WSACleanup();
		}
	};
#endif
	enum class Protocol { UDP, TCP };
	enum class AddressType { IPV4, IPV6 };

	template <Protocol protocol, AddressType addr_type>
	class Socket
	{
	public:
		Socket()
		{
			if constexpr (addr_type == AddressType::IPV4 && protocol == Protocol::TCP)
			{
				m_socket = socket(AF_INET, SOCK_STREAM, 0);
			}
			else if constexpr (addr_type == AddressType::IPV4 && protocol == Protocol::UDP)
			{
				m_socket = socket(AF_INET, SOCK_DGRAM, 0);
			}
			else if constexpr (addr_type == AddressType::IPV6 && protocol == Protocol::TCP)
			{
				m_socket = socket(AF_INET6, SOCK_STREAM, 0);
			}
			else
			{
				m_socket = socket(AF_INET6, SOCK_DGRAM, 0);
			}
		}

		explicit Socket(SOCKET s) : m_socket(s) {}

		Socket(const Socket& other)
		{
			m_socket = other.m_socket;
		}

		Socket(Socket&& other) noexcept
		{
			m_socket = other.m_socket;
			other.m_socket = INVALID_SOCKET;
		}

		Socket& operator= (const Socket& other) = default;

		~Socket()
		{
			//Close();
		}

		void Close()
		{
			if (m_socket == INVALID_SOCKET) return;

			// Shutdown connection if TCP
			if (protocol == Protocol::TCP)
			{
				shutdown(m_socket, SHUT_RDWR);
			}

			// Closing socket
			close(m_socket);
			m_socket = INVALID_SOCKET;
		}

		bool Bind(const std::string& interf, uint16_t port)
		{
			if constexpr (protocol == Protocol::UDP) return false;
			if (m_socket == INVALID_SOCKET) return false;

			auto sockAddr = ParseEndpoint(interf, port);
			int err = bind(m_socket, (sockaddr*)&sockAddr, sizeof(sockAddr));

			return (err == 0);
		}

		bool Listen(uint16_t backlog)
		{
			if constexpr (protocol == Protocol::UDP) return false;
			if (m_socket == INVALID_SOCKET) return false;

			int err = listen(m_socket, backlog);
			return (err == 0);
		}

		std::optional<Socket> Accept()
		{
			if constexpr (protocol == Protocol::UDP) return {};
			if (m_socket == INVALID_SOCKET) return {};

			sockaddr clientAddr;
			int clientAddrLen = sizeof(clientAddr);
			memset(&clientAddr, 0, clientAddrLen);
			SOCKET client = accept(m_socket, &clientAddr, &clientAddrLen);

			if (client == INVALID_SOCKET) return {};

			return Socket<protocol, addr_type>(client);
		}

		bool Connect(const std::string& addr, uint16_t port)
		{
			if constexpr(protocol == Protocol::UDP) return false;
			if (m_socket == INVALID_SOCKET) return false;

			auto sockAddr = ParseEndpoint(addr, port);
			int err = connect(m_socket, (sockaddr*)&sockAddr, sizeof(sockAddr));
			return (err == 0);
		}

		// Send function in TCP mode
		std::optional<size_t> Send(const char* data, size_t dataLen)
		{
			if constexpr (protocol == Protocol::UDP) return {};
			if (m_socket == INVALID_SOCKET) return {};

			size_t size = send(m_socket, data, dataLen, 0);
			if (size < 0) return {};
			return size;
		}

		// Receive function in TCP mode
		std::optional<size_t> Receive(char* data, size_t dataLen)
		{
			if constexpr (protocol == Protocol::UDP) return {};
			if (m_socket == INVALID_SOCKET) return {};

			size_t size = recv(m_socket, data, dataLen, 0);
			if (size < 0) return {};
			return size;
		}

		// Send function in UDP mode
		std::optional<size_t> SendTo(const std::string& addr, uint16_t port, const char* data, size_t dataLen)
		{
			if constexpr (protocol == Protocol::TCP) return {};
			if (m_socket == INVALID_SOCKET) return {};

			auto endpoint = ParseEndpoint(addr, port);
			size_t size = sendto(m_socket, data, dataLen, 0, (sockaddr*)&endpoint, sizeof(endpoint));
			if (size < 0) return {};
			return size;
		}

		// Receive function in UDP mode
		std::optional<size_t> ReceiveFrom(const std::string& addr, uint16_t port, const char* data, size_t dataLen)
		{
			if constexpr (protocol == Protocol::TCP) return {};
			if (m_socket == INVALID_SOCKET) return {};

			auto endpoint = ParseEndpoint(addr, port);
			size_t size = recvfrom(m_socket, data, dataLen, 0, (sockaddr*)&endpoint, sizeof(endpoint));
			if (size < 0) return {};
			return size;
		}

		int GetLastError()
		{
			return errno();
		}

		static bool Select(std::vector<Socket<protocol, addr_type>>& read, 
						   std::vector<Socket<protocol, addr_type>>& write,
						   std::vector<Socket<protocol, addr_type>>& except)
		{
			// Init socket sets
			fd_set readSet;
			FD_ZERO(&readSet);
			fd_set writeSet;
			FD_ZERO(&writeSet);
			fd_set exceptSet;
			FD_ZERO(&exceptSet);

			// Add sockets to sets
			for (auto& sock : read)
			{
				FD_SET(sock.m_socket, &readSet);
			}
			for (auto& sock : write)
			{
				FD_SET(sock.m_socket, &writeSet);
			}
			for (auto& sock : except)
			{
				FD_SET(sock.m_socket, &exceptSet);
			}

			// Call SELECT
			int ret = select(0, &readSet, &writeSet, &exceptSet, nullptr) == 0;

			// Clean sockets that are not selected
			for (int i = 0; i < read.size(); ++i)
			{
				if (FD_ISSET(read[i].m_socket, &readSet)) continue;
				read.erase(read.begin() + i);
				--i;
			}
			for (int i = 0; i < write.size(); ++i)
			{
				if (FD_ISSET(write[i].m_socket, &writeSet)) continue;
				write.erase(write.begin() + i);
				--i;
			}
			for (int i = 0; i < except.size(); ++i)
			{
				if (FD_ISSET(except[i].m_socket, &exceptSet)) continue;
				except.erase(except.begin() + i);
				--i;
			}

			return ret == 0;
		}

	private:
		static constexpr auto ParseEndpoint(const std::string& addr, uint16_t port)
		{
			if constexpr (addr_type == AddressType::IPV4)
			{
				// Create Infos
				sockaddr_in socketaddr;
				socketaddr.sin_family = AF_INET;
				socketaddr.sin_port = htons(port);
				inet_pton(socketaddr.sin_family, addr.c_str(), &socketaddr.sin_addr);

				return socketaddr;
			}
			else
			{
				// Create Infos
				sockaddr_in6 socketaddr;
				socketaddr.sin_family = AF_INET6;
				socketaddr.sin_port = htons(port);
				inet_pton(socketaddr.sin_family, addr.c_str(), &socketaddr.sin_addr);

				return socketaddr;
			}
		}

		SOCKET m_socket = INVALID_SOCKET;

		#ifdef _WIN32
		static WinInitializer ms_initializer;
		#endif
	};

#ifdef _WIN32
	WinInitializer Socket<Protocol::TCP, AddressType::IPV4>::ms_initializer;
	WinInitializer Socket<Protocol::UDP, AddressType::IPV4>::ms_initializer;
	WinInitializer Socket<Protocol::TCP, AddressType::IPV6>::ms_initializer;
	WinInitializer Socket<Protocol::UDP, AddressType::IPV6>::ms_initializer;
#endif
}
