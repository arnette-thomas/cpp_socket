#include <atomic>
#include <chrono>
using namespace std::literals::chrono_literals;
#include <iostream>
#include <thread>
#include <vector>
#include <memory>

#include<cpp_socket/cpp_socket.hpp>

/**
* Server example with multithreading and non-blocking sockets
*/

std::atomic_bool running = true;

std::vector<sock::Socket<sock::Protocol::TCP, sock::AddressType::IPV4>> mg_clients;

class SpinLockGuard
{
public:
	explicit SpinLockGuard(std::atomic_flag& flag) : m_flag(flag)
	{
		while (m_flag.test_and_set(std::memory_order_acquire));
	}

	~SpinLockGuard()
	{
		m_flag.clear(std::memory_order_release);
	}

	void notify()
	{
		ms_notified = true;
	}

	bool check()
	{
		bool oldValue = ms_notified;
		ms_notified = false;
		return oldValue;
	}

private:
	std::atomic_flag& m_flag;
	static std::atomic_bool ms_notified;
};
std::atomic_bool SpinLockGuard::ms_notified;

std::atomic_flag globalSpinLock = ATOMIC_FLAG_INIT;

void ReceiveRoutine()
{
	std::vector<sock::Socket<sock::Protocol::TCP, sock::AddressType::IPV4>> mg_read;
	std::vector<sock::Socket<sock::Protocol::TCP, sock::AddressType::IPV4>> mg_write;
	std::vector<sock::Socket<sock::Protocol::TCP, sock::AddressType::IPV4>> mg_except;

	while (running)
	{
		{
			SpinLockGuard lock(globalSpinLock);
			if (!lock.check()) continue;
			int id = 0;
			for (int i = 0; i < mg_clients.size();)
			{
				auto& s = mg_clients[i];
				mg_read.push_back(s);
				std::cout << id << " - Handling client" << std::endl;
				mg_clients.erase(mg_clients.begin() + i);
				++id;
			}
		}
		if (!sock::Socket<sock::Protocol::TCP, sock::AddressType::IPV4>::Select(mg_read, mg_write, mg_except)) return;
		for (int i = 0; i < mg_read.size();)
		{
			auto& s = mg_read[i];
			char buffer[1024];
			auto received = s.Receive(buffer, 1024);

			if (!received)
			{
				std::cout << "Error in receive : " << s.GetLastError() << std::endl;
				return;
			}

			// std::this_thread::sleep_for(5s);
			std::string toSend("Hello from server !");
			auto sent = s.Send(toSend.c_str(), toSend.size());

			if (!sent)
			{
				std::cout << "Error in sending : " << s.GetLastError() << std::endl;
				return;
			}

			s.Close();
			mg_read.erase(mg_read.begin() + i);
		}
	}
}

void ServerRoutine(sock::Socket<sock::Protocol::TCP, sock::AddressType::IPV4>& server)
{
	while (running)
	{
		if (auto client = server.Accept(); client.has_value())
		{
			{
				SpinLockGuard lock(globalSpinLock);
				mg_clients.push_back(client.value());
				lock.notify();
				std::cout << "Adding client to list" << std::endl;
			}
		}
		else
		{
			std::cout << "Error in accept : " << server.GetLastError() << std::endl;
			return;
		}
	}

}
int main(int argc, char* argv[])
{
	sock::Socket<sock::Protocol::TCP, sock::AddressType::IPV4> server;
	if (!server.Bind("127.0.0.1", 8888))
	{
		std::cout << "Error in bind : " << server.GetLastError() << std::endl;
		return EXIT_FAILURE;
	}

	if (!server.Listen(255))
	{
		std::cout << "Error in listen : " << server.GetLastError() << std::endl;
		return EXIT_FAILURE;
	}

	std::thread serverThread([&server]() {ServerRoutine(server); });
	std::thread receiveThread([&server]() {ReceiveRoutine(); });

	std::cout << "Press enter to quit" << std::endl;

	std::cin.ignore();

	running = false;
	server.Close();

	if (serverThread.joinable()) serverThread.join();
	if (receiveThread.joinable()) receiveThread.join();

	std::cin.ignore();
}